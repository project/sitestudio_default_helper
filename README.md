# Site Studio Default Helper

The Site Studio Default Helper module allows site builders to select a default
helper for a layout canvas field.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sitestudio_default_helper).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sitestudio_default_helper).

## Table of contents

- Requirements
- Installation
- Usage
- Maintainers

## Requirements

This module requires the following modules:
- [Acquia Site Studio](https://sitestudiodocs.acquia.com/7.0/user-guide/step-1a-install-site-studio-modules-composer)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Usage
- Create a [helper](https://sitestudiodocs.acquia.com/7.0/user-guide/creating-and-using-helpers) in Site Studio.
- Navigate to an entity with a layout_canvas field (e.g a node content type).
- Go to the "Form Display" tab.
- Click the cog wheel on the layout canvas field.
- Select the default helper you want to load when the field first loads.

## Maintainers
- Pavlos Daniel - [pavlosdan](https://www.drupal.org/u/pavlosdan)
